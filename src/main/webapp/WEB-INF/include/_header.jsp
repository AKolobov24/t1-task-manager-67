<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>TASK MANAGER</title>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="10" border="1" style="border-collapse: collapse; ">
            <tr height="35">
                <td width="50%">
                    <b>TASK MANAGER</b>
                </td>
                <td width="50%" align="right">
                    <a href="/projects">PROJECTS</a>
                        |
                    <a href="/tasks">TASKS</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="vertical-align: top;">
