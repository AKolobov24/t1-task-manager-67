package ru.t1.akolobov.tm.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.akolobov.tm.web.configuration.ApplicationConfiguration;
import ru.t1.akolobov.tm.web.configuration.WebApplicationConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class};
    }

    @NotNull
    @Override
    protected String @NotNull [] getServletMappings() {
        return new String[]{"/"};
    }

}
