package ru.t1.akolobov.tm.web.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.model.AbstractModel;

import java.util.Collection;

public interface IService<M extends AbstractModel> {

    @NotNull
    Collection<M> findAll();

    @Nullable
    M findById(@NotNull final String id);

    @NotNull
    M save(@NotNull final M model);

    void deleteById(@NotNull final String id);

}
