package ru.t1.akolobov.tm.web.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.web.model.Task;

@Repository
@Scope("prototype")
public interface TaskRepository extends CommonRepository<Task> {

}
