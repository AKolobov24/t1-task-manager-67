package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.ProjectEndpoint;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@WebService(endpointInterface = "ru.t1.akolobov.tm.web.api.endpoint.ProjectEndpoint")
@RequestMapping("/api/projects")
public final class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @WebMethod
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id")
            @NotNull final String id
    ) {
        return projectService.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody
            @NotNull final Project project
    ) {
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id")
            @NotNull final String id
    ) {
        projectService.deleteById(id);
    }

}
