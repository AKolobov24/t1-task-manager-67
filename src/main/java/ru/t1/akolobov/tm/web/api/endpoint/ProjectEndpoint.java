package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @NotNull
    @WebMethod
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    );

    @Nullable
    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project")
            @RequestBody
            @NotNull final Project project
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    );

}
