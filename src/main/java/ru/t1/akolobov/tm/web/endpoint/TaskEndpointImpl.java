package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.TaskEndpoint;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@WebService(endpointInterface = "ru.t1.akolobov.tm.web.api.endpoint.TaskEndpoint")
@RequestMapping("/api/tasks")
public final class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @WebMethod
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    ) {
        return taskService.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task")
            @RequestBody
            @NotNull final Task task
    ) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    ) {
        taskService.deleteById(id);
    }

}
