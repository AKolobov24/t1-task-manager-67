package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {

    @NotNull
    @WebMethod
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<Task> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    );

    @Nullable
    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task")
            @RequestBody
            @NotNull final Task task
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id")
            @NotNull final String id
    );

}
