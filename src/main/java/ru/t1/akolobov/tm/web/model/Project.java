package ru.t1.akolobov.tm.web.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tm_project", schema = "web")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractModel {

    public Project() {
        this.setName("New Project");
    }

    public Project(@NotNull final String name) {
        this.setName(name);
    }

}
