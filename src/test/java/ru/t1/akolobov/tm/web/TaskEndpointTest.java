package ru.t1.akolobov.tm.web;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.web.client.TaskRestClient;
import ru.t1.akolobov.tm.web.marker.IntegrationCategory;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.Arrays;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final TaskRestClient taskRestClient = TaskRestClient.client();

    @NotNull
    private final Task task1 = new Task("test-task-1");

    @NotNull
    private final Task task2 = new Task("test-task-2");

    @NotNull
    private final Task task3 = new Task("test-task-3");

    @Before
    public void addData() {
        taskRestClient.save(task1);
        taskRestClient.save(task2);
        taskRestClient.save(task3);
    }

    @After
    public void clearData() {
        if (taskRestClient.findById(task1.getId()) != null)
            taskRestClient.deleteById(task1.getId());
        if (taskRestClient.findById(task2.getId()) != null)
            taskRestClient.deleteById(task2.getId());
        if (taskRestClient.findById(task3.getId()) != null)
            taskRestClient.deleteById(task3.getId());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(
                taskRestClient
                        .findAll()
                        .containsAll(Arrays.asList(task1, task2, task3))
        );
    }

    @Test
    public void findById() {
        Assert.assertEquals(task1, taskRestClient.findById(task1.getId()));
        Assert.assertEquals(task2, taskRestClient.findById(task2.getId()));
        Assert.assertEquals(task3, taskRestClient.findById(task3.getId()));
    }

    @Test
    public void save() {
        @NotNull Task changedTask = new Task();
        @NotNull final Task newTask = new Task("new-task");
        changedTask.setId(task1.getId());
        changedTask.setName("test-task-changed");
        taskRestClient.save(changedTask);
        taskRestClient.save(newTask);
        Assert.assertEquals(changedTask, taskRestClient.findById(task1.getId()));
        Assert.assertEquals(newTask, taskRestClient.findById(newTask.getId()));
        taskRestClient.deleteById(newTask.getId());
    }

    @Test
    public void deleteById() {
        taskRestClient.deleteById(task1.getId());
        Assert.assertNull(taskRestClient.findById(task1.getId()));
        Assert.assertFalse(taskRestClient.findAll().contains(task1));
    }
}
